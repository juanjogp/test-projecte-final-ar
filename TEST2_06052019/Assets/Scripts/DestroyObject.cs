﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyObject : MonoBehaviour
{
    public float delayDestroy = 3.0f;
    public Text score;
    public int BulletDMG = 1;

    private int points;

    Wood wood;
    Stone stone;

    void OnCollisionEnter(Collision col)
    {
        // Destroy object on collision with the tag prop
        // In our case the gameobjects of the structure
        if (col.gameObject.tag == "Prop")
        {
            int intScore = int.Parse(score.text);
            switch (col.gameObject.name)
            {
                case "Stone":
                    stone = col.gameObject.GetComponent<Stone>();
                    stone.Hit(BulletDMG);
                    Debug.Log("Stone Hit");
                    intScore += stone.hitScore;
                    Debug.Log("SCORE: "+intScore);
                    break;
                case "Wood":
                    wood = col.gameObject.GetComponent<Wood>();
                    wood.Hit(BulletDMG);
                    Debug.Log("Wood Hit");
                    intScore += wood.hitScore;
                    Debug.Log("SCORE: " + intScore);
                    break;
                default:
                    Debug.Log("Unknow Object Hit");
                    break;
            }

            score.text = "" + intScore;
            //Debug.Log("Object Destroyed");
            //Destroy(col.gameObject, delayDestroy);
        }
    }

    void SetCountText()
    {
        // score.text = "Count: "+
    }
}
