﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stone : MonoBehaviour
{
    public int hpBlock = 3;
    public float delayDestroy = 3.0f;
    public float delayPop = 5.0f;
    public int hitScore = 150;

    public GameObject scorePS;
    public GameObject destroyPS;
    //TODO Al romper el bloque te da mas puntos (el doble?)

    private int currentHP;

    void Awake()
    {
        currentHP = hpBlock;
    }

    public void Hit(int dmg)
    {
        currentHP -= dmg;
        Debug.Log("-----Stone HP:" + currentHP);
        GameObject popup = Instantiate(scorePS, transform.position, transform.rotation);
        Destroy(popup, delayPop);
        if (currentHP <= 0)
        {
            GameObject destroy = Instantiate(destroyPS, transform.position, transform.rotation);
            Destroy(gameObject, delayDestroy);
            Debug.Log("Stone Destroy");
            Destroy(destroy, delayDestroy + 1);
        }
    }
}
