﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wood : MonoBehaviour
{
    public int hpBlock = 2;
    public float delayDestroy = 0.1f;
    public float delayPop = 5.0f;
    public int hitScore = 50;

    public GameObject scorePS;
    public GameObject destroyPS;
    //TODO Al romper el bloque te da mas puntos (el doble?)

    private int currentHP;

    void Awake()
    {
        currentHP = hpBlock;
    }

    public void Hit(int dmg)
    {
        currentHP -= dmg;
        Debug.Log("-----Wood HP:"+currentHP);
        GameObject popup = Instantiate(scorePS, transform.position, transform.rotation);
        Destroy(popup, delayPop);
        if (currentHP <= 0)
        {
            GameObject destroy = Instantiate(destroyPS, transform.position, transform.rotation);
            Destroy(gameObject, delayDestroy);
            Debug.Log("Wood Destroy");
            Destroy(destroy, delayDestroy + 1);
        }
    }
}
