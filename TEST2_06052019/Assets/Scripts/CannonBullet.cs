﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBullet : MonoBehaviour
{
    public Rigidbody bullet;
    public float velocidad = 15f;

    void Update()
    {
        if (Input.GetButtonUp("Fire1"))
        {
            Rigidbody inst = Instantiate(bullet, transform.position, transform.rotation);
            inst.velocity = transform.TransformDirection(new Vector3(0, 0, velocidad));
            // Physics.IgnoreCollision(inst.GetComponent<Collider>(), GetComponent<Collider>());
        }
                
    }
}
